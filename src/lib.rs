#![cfg_attr(not(test), no_std)]
#![feature(
    maybe_uninit_slice,
    maybe_uninit_extra,
    maybe_uninit_uninit_array,
    maybe_uninit_ref
)]

use core::convert::TryInto;
use core::default::Default;
use core::fmt;
use core::iter::IntoIterator;
use core::mem::MaybeUninit;
use core::ops::{Drop, Index, IndexMut};
use core::ptr;

pub mod iterators;
mod utils;
use utils::eremc;

pub struct CircularBuffer<T, const N: usize> {
    start: isize,
    end: isize,

    /// The only valid (initialized and owned) items are those of id:
    /// self.start % N
    /// self.start + 1 % N
    /// up to
    /// self.end -1 % included
    buffer: [MaybeUninit<T>; N],
}

impl<T, const N: usize> CircularBuffer<T, N> {
    const N_ISIZE: isize = N as isize;

    pub fn new() -> Self {
        let tmp: Result<isize, _> = N.try_into();
        assert!(
            N != 0 && tmp.is_ok(),
            "CircularBuffer doesn't support zero sized buffer or \
            buffer sizes that can't be represented as a isize."
        );

        CircularBuffer {
            start: 0,
            end: 0,
            buffer: MaybeUninit::uninit_array(),
        }
    }

    pub fn start(&self) -> isize {
        self.start
    }

    pub fn end(&self) -> isize {
        self.end
    }

    pub fn is_empty(&self) -> bool {
        self.end == self.start
    }

    pub fn is_full(&self) -> bool {
        self.end - self.start == Self::N_ISIZE
    }

    /// Rearanges the buffer so that the all items in it are stored in order, in a single
    /// continuous memory region which is returned as a slice
    ///
    /// After that, the start of the buffer is index 0
    pub fn linearize(&mut self) -> &mut [T] {
        let rstart = self.rstart();
        self.buffer.rotate_left(rstart);
        self.end = self.len() as isize;
        self.start = 0;
        return unsafe {
            MaybeUninit::slice_assume_init_mut(&mut self.buffer[0..self.end as usize])
        };
    }

    fn real_index_from_out(&self, idx: usize) -> usize {
        self.real_index(idx as isize + self.start)
    }

    fn real_index(&self, idx: isize) -> usize {
        eremc(idx, N)
    }

    fn rstart(&self) -> usize {
        eremc(self.start, N)
    }

    fn rend(&self) -> usize {
        eremc(self.end, N)
    }

    fn is_linear(&self) -> bool {
        eremc(self.start, N) <= eremc(self.end - 1, N)
    }

    #[allow(unused_unsafe)]
    unsafe fn get_assume_init_ref(&self, idx: usize) -> &T {
        unsafe { self.buffer[idx].assume_init_ref() }
    }

    #[allow(unused_unsafe)]
    unsafe fn get_assume_init_mut(&mut self, idx: usize) -> &mut T {
        unsafe { self.buffer[idx].assume_init_mut() }
    }

    #[allow(unused_unsafe)]
    unsafe fn get_assume_init(&self, idx: usize) -> T {
        let mut tmp = MaybeUninit::uninit();
        unsafe {
            ptr::copy_nonoverlapping(&self.buffer[idx], &mut tmp, 1);
            tmp.assume_init()
        }
    }

    pub fn as_slices(&self) -> (&[T], &[T]) {
        if self.is_linear() {
            unsafe {
                (
                    MaybeUninit::slice_assume_init_ref(
                        &self.buffer[eremc(self.start, N)..eremc(self.end - 1, N) + 1],
                    ),
                    MaybeUninit::slice_assume_init_ref(&self.buffer[0..0]),
                )
            }
        } else {
            unsafe {
                (
                    MaybeUninit::slice_assume_init_ref(&self.buffer[eremc(self.start, N)..]),
                    MaybeUninit::slice_assume_init_ref(&self.buffer[..eremc(self.end, N)]),
                )
            }
        }
    }

    pub fn as_mut_slices(&mut self) -> (&mut [T], &mut [T]) {
        let linear = self.is_linear();
        let (tmp_start, tmp_remain) = self.buffer.split_at_mut(eremc(self.end - 1, N) + 1);
        if linear {
            unsafe {
                (
                    MaybeUninit::slice_assume_init_mut(&mut tmp_start[eremc(self.start, N)..]),
                    MaybeUninit::slice_assume_init_mut(&mut tmp_remain[0..0]),
                )
            }
        } else {
            unsafe {
                (
                    MaybeUninit::slice_assume_init_mut(&mut tmp_start[eremc(self.start, N)..]),
                    MaybeUninit::slice_assume_init_mut(
                        &mut tmp_remain[..eremc(self.end - 1, N) + 1],
                    ),
                )
            }
        }
    }

    /// Used to reduce "start" and "end" to prevent them from overflowing isize
    /// This can be critical in 32bit architecture and lead to UB
    fn avoid_overflow(&mut self) -> bool {
        if self.start < -4 * Self::N_ISIZE  {
            self.start += 4 * Self::N_ISIZE;
            self.end+= 4 * Self::N_ISIZE;
            true
        } else if self.end > 4 * Self::N_ISIZE {
            self.start -= 4 * Self::N_ISIZE;
            self.end -= 4 * Self::N_ISIZE;
            true
        } else {
            false
        }
    }

    /// isize because it is easier to then use for other len calculations
    pub fn len(&self) -> usize {
        (self.end - self.start) as usize
    }

    /// Get a reference of item at index i, None if i is out of bounds
    pub fn get(&self, i: usize) -> Option<&T> {
        if i < self.len() {
            Some(unsafe { self.get_assume_init_ref(self.real_index_from_out(i)) })
        } else {
            None
        }
    }

    /// Get a mutable reference of item at index i, None if i is out of bounds
    pub fn get_mut(&mut self, i: usize) -> Option<&mut T> {
        if i < self.len() {
            Some(unsafe { self.get_assume_init_mut(self.real_index_from_out(i)) })
        } else {
            None
        }
    }

    /// Append an element to the back of the buffer
    /// If the buffer is full, it overwrites the first item and returns it
    /// Returns None otherwise
    pub fn push_back(&mut self, item: T) -> Option<T> {
        let to_ret = if self.is_full() {
            // The buffer is full, we need to store the overwritten value to return it
            self.start += 1;
            Some(unsafe { self.get_assume_init(self.real_index(self.start - 1)) })
        } else {
            None
        };

        self.buffer[self.rend()] = MaybeUninit::new(item);
        self.end += 1;
        self.avoid_overflow();
        to_ret
    }

    /// Append an element to the front of the buffer
    /// If the buffer is full, it overwrites the last item and returns it
    /// Returns None otherwise
    pub fn push_front(&mut self, item: T) -> Option<T> {
        let to_ret = if self.is_full() {
            // The buffer is full, we need to store the overwritten value to return it
            self.end -= 1;
            Some(unsafe { self.get_assume_init(self.rend()) })
        } else {
            None
        };

        self.start -= 1;
        self.buffer[self.rstart()] = MaybeUninit::new(item);
        self.avoid_overflow();
        to_ret
    }

    /// Pop the last element of the buffer
    /// if it is empty, returns none
    pub fn pop_back(&mut self) -> Option<T> {
        if self.is_empty() {
            return None;
        }

        // The item at index buffer[eremc(end ,N)] is not valid, so we decrement first
        self.end -= 1;
        Some(unsafe { self.get_assume_init(self.rend()) })
    }

    /// Pop the last element of the buffer
    /// if it is empty, returns none
    pub fn pop_front(&mut self) -> Option<T> {
        if self.is_empty() {
            return None;
        }

        self.start += 1;
        Some(unsafe { self.get_assume_init(self.real_index(self.start - 1)) })
    }

    pub fn iter(&self) -> iterators::CircularBufferRefIter<'_, T> {
        self.into()
    }

    pub fn iter_mut(&mut self) -> iterators::CircularBufferMutIter<'_, T> {
        self.into()
    }
}

impl<T, const N: usize> Default for CircularBuffer<T, N> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T, const N: usize> Index<usize> for CircularBuffer<T, N> {
    type Output = T;

    /// Get the value at index i
    ///
    /// Panics if out of bounds
    fn index(&self, i: usize) -> &T {
        self.get(i).expect("Index out of bounds")
    }
}

impl<T, const N: usize> IndexMut<usize> for CircularBuffer<T, N> {
    /// Get a mutable reference to the value at index i
    ///
    /// Panics if out of bounds
    fn index_mut(&mut self, i: usize) -> &mut T {
        self.get_mut(i).expect("Index out of bounds")
    }
}

impl<T, const N: usize> Drop for CircularBuffer<T, N> {
    fn drop(&mut self) {
        for i in self.start..self.end {
            unsafe {
                self.buffer[eremc(i, N)].assume_init_drop();
            }
        }
    }
}

impl<'a, T, const N: usize> IntoIterator for &'a CircularBuffer<T, N> {
    type Item = &'a T;
    type IntoIter = iterators::CircularBufferRefIter<'a, T>;

    fn into_iter(self) -> Self::IntoIter {
        self.into()
    }
}

impl<'a, T, const N: usize> IntoIterator for &'a mut CircularBuffer<T, N> {
    type Item = &'a mut T;
    type IntoIter = iterators::CircularBufferMutIter<'a, T>;

    fn into_iter(self) -> Self::IntoIter {
        self.into()
    }
}

impl<T, const N: usize> IntoIterator for CircularBuffer<T, N> {
    type Item = T;
    type IntoIter = iterators::IntoIter<T, N>;

    fn into_iter(self) -> Self::IntoIter {
        self.into()
    }
}

impl<T: fmt::Debug, const N: usize> fmt::Debug for CircularBuffer<T, N> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut list_dbg = f.debug_list();
        for i in self.iter() {
            list_dbg.entry(i);
        }
        list_dbg.finish()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn fill() {
        let mut buf = CircularBuffer::<usize, 10>::new();
        for i in 0..10 {
            assert!(buf.push_back(i).is_none());
        }

        for i in 0..10 {
            assert_eq!(i, buf[i]);
        }

        for i in 0..10 {
            assert_eq!(Some(9 - i), buf.pop_back());
        }

        assert!(buf.pop_back().is_none());
    }

    #[test]
    fn fill_front() {
        let mut buf = CircularBuffer::<usize, 10>::new();
        assert!(buf.is_empty());
        for i in 0..10 {
            assert!(buf.push_front(i).is_none());
        }

        for i in 0..10 {
            assert_eq!(9 - i, buf[i]);
        }

        for i in 0..10 {
            assert!(!buf.is_empty());
            assert_eq!(Some(9 - i), buf.pop_front());
        }

        assert!(buf.pop_back().is_none());
        assert!(buf.is_empty());
    }

    #[test]
    fn linearize() {
        let mut buf = CircularBuffer::<usize, 10>::new();
        assert!(buf.is_linear());
        for i in 0..10 {
            assert!(buf.push_back(i).is_none());
            assert!(buf.is_linear());
        }

        for i in 10..15 {
            assert_eq!(buf.push_back(i), Some(i - 10));
            assert!(!buf.is_linear());
        }

        assert_eq!(
            buf.as_slices(),
            (&*vec![5, 6, 7, 8, 9], &*vec![10, 11, 12, 13, 14])
        );

        assert!(!buf.is_linear());
        assert_eq!(
            buf.linearize(),
            &mut *vec![5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
        );
        assert!(buf.is_linear());
        assert_eq!(
            buf.as_mut_slices(),
            (&mut *vec![5, 6, 7, 8, 9, 10, 11, 12, 13, 14], &mut *vec![])
        );
        assert_eq!(
            buf.as_slices(),
            (&*vec![5, 6, 7, 8, 9, 10, 11, 12, 13, 14], &*vec![])
        );
    }

    #[test]
    fn linearize_linear() {
        let mut buf = CircularBuffer::<usize, 10>::new();
        assert!(buf.is_linear());
        for i in 0..7 {
            assert!(buf.push_back(i).is_none());
            assert!(buf.is_linear());
        }

        assert_eq!(buf.as_slices(), (&*vec![0, 1, 2, 3, 4, 5, 6], &*vec![]));
        assert_eq!(buf.start(), 0);
        assert_eq!(buf.end(), 7);
        assert_eq!(
            buf.as_mut_slices(),
            (&mut *vec![0, 1, 2, 3, 4, 5, 6], &mut *vec![])
        );
        buf.pop_front();
        buf.pop_front();

        assert_eq!(buf.as_slices(), (&*vec![2, 3, 4, 5, 6], &*vec![]));
        assert_eq!(buf.start(), 2);
        assert_eq!(buf.end(), 7);
        assert_eq!(
            buf.as_mut_slices(),
            (&mut *vec![2, 3, 4, 5, 6], &mut *vec![])
        );

        buf.linearize();
        assert_eq!(buf.as_slices(), (&*vec![2, 3, 4, 5, 6], &*vec![]));
        assert_eq!(buf.start(), 0);
        assert_eq!(buf.end(), 5);
        assert_eq!(
            buf.as_mut_slices(),
            (&mut *vec![2, 3, 4, 5, 6], &mut *vec![])
        );
    }

    fn generic_overflow<const N: usize>() {
        let mut buf = CircularBuffer::<usize, N>::new();
        for i in 0..N {
            assert!(!buf.is_full());
            assert!(buf.push_back(i).is_none());
        }
        assert!(buf.is_full());

        for i in 0..100 * N {
            assert_eq!(buf.push_back(i + N), Some(i));
            assert!(buf.is_full());
        }

        for i in 0..N {
            assert_eq!(buf[i], i + 100 * N);
        }
    }

    #[test]
    fn overflow() {
        generic_overflow::<1>();
        generic_overflow::<7>();
        generic_overflow::<8>();
        generic_overflow::<9>();
        generic_overflow::<100>();
        generic_overflow::<31>();
        generic_overflow::<32>();
        generic_overflow::<33>();
    }

    #[test]
    fn pop_front() {
        let mut buf = CircularBuffer::<usize, 10>::new();
        for i in 0..10 {
            assert!(buf.push_back(i).is_none());
        }

        for i in 0..5 {
            assert_eq!(Some(i), buf.pop_front());
        }

        for i in 0..5 {
            assert_eq!(Some(9 - i), buf.pop_back());
        }

        assert!(buf.pop_back().is_none());
        assert!(buf.pop_front().is_none());
    }

    #[test]
    fn slices() {
        let mut buf = CircularBuffer::<usize, 10>::new();
        for i in 0..5 {
            assert!(buf.push_back(i).is_none());
        }

        assert_eq!(buf.as_slices(), (&*vec![0, 1, 2, 3, 4,], &*vec![]));
        assert_eq!(
            buf.as_mut_slices(),
            (&mut *vec![0, 1, 2, 3, 4,], &mut *vec![])
        );

        for i in 5..10 {
            assert!(buf.push_back(i).is_none());
        }

        assert_eq!(
            buf.as_slices(),
            (&*vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9], &*vec![])
        );
        assert_eq!(
            buf.as_mut_slices(),
            (&mut *vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9], &mut *vec![])
        );

        for i in 0..5 {
            assert_eq!(buf.push_back(i + 10), Some(i));
        }

        assert_eq!(
            buf.as_slices(),
            (&*vec![5, 6, 7, 8, 9], &*vec![10, 11, 12, 13, 14])
        );
    }

    #[test]
    fn debug() {
        let mut buf = CircularBuffer::<usize, 10>::new();
        for i in 0..5 {
            buf.push_back(i);
        }

        assert_eq!(format!("{:?}", buf), format!("{:?}", vec![0, 1, 2, 3, 4]));

        for i in 5..10 {
            buf.push_back(i);
        }

        assert_eq!(
            format!("{:?}", buf),
            format!("{:?}", vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
        );

        for i in 10..15 {
            buf.push_back(i);
        }

        assert_eq!(
            format!("{:?}", buf),
            format!("{:?}", vec![5, 6, 7, 8, 9, 10, 11, 12, 13, 14])
        );
    }

    #[test]
    fn large() {
        let mut buf = CircularBuffer::<usize, 100>::new();
        for i in 0..100 {
            assert!(!buf.is_full());
            assert!(buf.push_back(i).is_none());
        }
        assert!(buf.is_full());

        for i in 0..100 {
            assert_eq!(i, buf[i]);
        }

        for i in 0..100 {
            assert_eq!(Some(99 - i), buf.pop_back());
        }

        assert!(buf.pop_back().is_none());
    }

    #[test]
    #[should_panic]
    fn zero_sized() {
        let _ = CircularBuffer::<usize, 0>::new();
    }
}
