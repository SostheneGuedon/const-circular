use super::CircularBuffer;
use core::convert::From;
use core::iter::{Chain, DoubleEndedIterator, ExactSizeIterator, Iterator};
use core::slice;

pub struct IntoIter<T, const N: usize> {
    inner: CircularBuffer<T, N>,
}

impl<T, const N: usize> From<CircularBuffer<T, N>> for IntoIter<T, N> {
    fn from(buf: CircularBuffer<T, N>) -> IntoIter<T, N> {
        IntoIter { inner: buf }
    }
}

impl<T, const N: usize> Iterator for IntoIter<T, N> {
    type Item = T;
    fn next(&mut self) -> Option<T> {
        self.inner.pop_front()
    }
}

impl<T, const N: usize> DoubleEndedIterator for IntoIter<T, N> {
    fn next_back(&mut self) -> Option<T> {
        self.inner.pop_back()
    }
}

impl<T, const N: usize> ExactSizeIterator for IntoIter<T, N> {
    fn len(&self) -> usize {
        // The len is known to be positive
        self.inner.len()
    }
}

// Use slices to implement the iterators, the main advantage being that it allows
// implementing iteration over mutable references without the need for any additionnal unsafe
//
// TODO: implement all special Iterator stuff that improve functionnality/performance that are
// already present in the slice iterators.
pub struct CircularBufferRefIter<'buf, T> {
    inner: Chain<slice::Iter<'buf, T>, slice::Iter<'buf, T>>,
}

impl<'buf, T, const N: usize> From<&'buf CircularBuffer<T, N>> for CircularBufferRefIter<'buf, T> {
    fn from(buf: &'buf CircularBuffer<T, N>) -> CircularBufferRefIter<'buf, T> {
        let (r, l) = buf.as_slices();
        CircularBufferRefIter {
            inner: r.iter().chain(l.iter()),
        }
    }
}

impl<'buf, T> Iterator for CircularBufferRefIter<'buf, T> {
    type Item = &'buf T;
    fn next(&mut self) -> Option<&'buf T> {
        self.inner.next()
    }
}

impl<'buf, T> DoubleEndedIterator for CircularBufferRefIter<'buf, T> {
    fn next_back(&mut self) -> Option<&'buf T> {
        self.inner.next_back()
    }
}

pub struct CircularBufferMutIter<'buf, T> {
    inner: Chain<slice::IterMut<'buf, T>, slice::IterMut<'buf, T>>,
}

impl<'buf, T, const N: usize> From<&'buf mut CircularBuffer<T, N>>
    for CircularBufferMutIter<'buf, T>
{
    fn from(buf: &'buf mut CircularBuffer<T, N>) -> CircularBufferMutIter<'buf, T> {
        let (r, l) = buf.as_mut_slices();
        CircularBufferMutIter {
            inner: r.iter_mut().chain(l.iter_mut()),
        }
    }
}

impl<'buf, T> Iterator for CircularBufferMutIter<'buf, T> {
    type Item = &'buf mut T;
    fn next(&mut self) -> Option<&'buf mut T> {
        self.inner.next()
    }
}

impl<'buf, T> DoubleEndedIterator for CircularBufferMutIter<'buf, T> {
    fn next_back(&mut self) -> Option<&'buf mut T> {
        self.inner.next_back()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn generic_fill<const N: usize>() {
        let mut buf = CircularBuffer::<usize, N>::new();
        for i in 0..N {
            assert!(buf.push_back(i).is_none());
        }

        for i in 0..N {
            assert_eq!(i, buf[i]);
        }

        for (i, j) in buf.iter().enumerate() {
            assert_eq!(&i, j);
        }

        assert_eq!(buf.iter().count(), N);
    }

    fn generic_fill_mut<const N: usize>() {
        let mut buf = CircularBuffer::<usize, N>::new();
        for i in 0..N {
            assert!(buf.push_back(i).is_none());
        }

        for i in 0..N {
            assert_eq!(i, buf[i]);
        }

        for (mut i, j) in buf.iter_mut().enumerate() {
            assert_eq!(&mut i, j);
        }

        assert_eq!(buf.iter().count(), N);
    }

    fn generic_fill_into<const N: usize>() {
        let mut buf = CircularBuffer::<usize, N>::new();
        for i in 0..N {
            assert!(buf.push_back(i).is_none());
        }

        for i in 0..N {
            assert_eq!(i, buf[i]);
        }

        for (i, j) in buf.into_iter().enumerate() {
            assert_eq!(i, j);
        }

        let mut buf = CircularBuffer::<usize, N>::new();
        for i in 0..N {
            assert!(buf.push_back(i).is_none());
        }
        assert_eq!(buf.iter().count(), N);
    }

    #[test]
    fn fill_front() {
        let mut buf = CircularBuffer::<usize, 10>::new();
        assert!(buf.is_empty());
        for i in 0..10 {
            assert!(buf.push_front(i).is_none());
        }

        for i in 0..10 {
            assert_eq!(9 - i, buf[i]);
        }

        for (idx, v) in buf.into_iter().enumerate() {
            assert_eq!(9 - idx, v);
        }
    }

    fn generic_overflow<const N: usize>() {
        let mut buf = CircularBuffer::<usize, N>::new();
        for i in 0..N {
            assert!(buf.push_back(i).is_none());
        }

        for i in 0..N {
            assert_eq!(buf.push_back(i + N), Some(i));
        }

        for (i, j) in buf.into_iter().enumerate() {
            assert_eq!(N + i, j);
        }
    }

    #[test]
    fn pop_front() {
        let mut buf = CircularBuffer::<usize, 10>::new();
        for i in 0..10 {
            assert!(buf.push_back(i).is_none());
        }

        for i in 0..5 {
            assert_eq!(Some(i), buf.pop_front());
        }

        assert_eq!(buf.into_iter().collect::<Vec<usize>>(), vec![5, 6, 7, 8, 9]);
    }

    #[test]
    fn nth() {
        let mut buf = CircularBuffer::<usize, 10>::new();
        for i in 0..15 {
            buf.push_back(i);
        }

        let mut iter = buf.into_iter();
        assert_eq!(iter.len(), 10);

        assert_eq!(iter.nth(0), Some(5));
        assert_eq!(iter.len(), 9);
        assert_eq!(iter.nth(1), Some(7));
        assert_eq!(iter.len(), 7);
        assert_eq!(iter.nth_back(1), Some(13));
        assert_eq!(iter.len(), 5);
        assert_eq!(iter.collect::<Vec<usize>>(), vec![8, 9, 10, 11, 12]);
    }

    #[test]
    fn fill() {
        generic_fill::<1>();
        generic_fill::<2>();
        generic_fill::<30>();
        generic_fill::<16>();
        generic_fill::<17>();
        generic_fill::<32>();
    }

    #[test]
    fn fill_mut() {
        generic_fill_mut::<1>();
        generic_fill_mut::<2>();
        generic_fill_mut::<30>();
        generic_fill_mut::<16>();
        generic_fill_mut::<17>();
        generic_fill_mut::<32>();
    }

    #[test]
    fn fill_into() {
        generic_fill_into::<1>();
        generic_fill_into::<2>();
        generic_fill_into::<30>();
        generic_fill_into::<16>();
        generic_fill_into::<17>();
        generic_fill_into::<32>();
    }

    #[test]
    fn overflow() {
        generic_overflow::<1>();
        generic_overflow::<2>();
        generic_overflow::<30>();
        generic_overflow::<16>();
        generic_overflow::<17>();
        generic_overflow::<32>();
    }
}
